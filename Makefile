UNAME_S := $(shell uname -s)
CXX :=

ifeq ($(UNAME_S),Linux)
$(info ************ Linux **********)
CXX=g++ 
endif
ifeq ($(UNAME_S),Darwin)
$(info ************ MacOS **********)
CXX=clang++ 
endif


CFLAGS = -std=c++1y -Wpedantic -Wall -Wextra -Wno-sign-compare   -O0
#CFLAGS = -std=c++1y   -O3
DEBUGCFLAGS = -std=c++1y -Wpedantic -Wall -Wextra -Wno-sign-compare -D_DEBUG_PRINT -g -O0 -ggdb3

# LIBS= -lm

CDEBUGCFLAGS := $(DEBUGCFLAGS) -D_DEBUG_CONCURRENCY_PRINT
QDEBUGCFLAGS := -std=c++1y -Wpedantic -g -O0 -ggdb3

IPATHS := -Iinclude

all:main

main: src/main.cc src/auxiliary.cc include/auxiliary.h
	$(CXX) $(CFLAGS) $(IPATHS) src/main.cc src/auxiliary.cc	 $(LIBS) -o example




clean:
	rm -rf example


