#include <stdio.h>
#include "auxiliary.h"

int main ()
{
    int i,K=10000000;
    int val;
    printf("Measuring in fine detail the latency of a loop\n");

    printf("%d iterations:\n",K);

    //define the clock variables to use to capute high precision time before and after the measured execution
    my_clock clk_start,clk_end;
    
    //capture the high-precision timestamp before executing the loop
    my_clock_get_time(&clk_start);
    
    //a loop with dummy calculations
    for (i=0;i<K;i++)
        val=2*i; 

    //capture the high-precision timestamp after executing the loop
    my_clock_get_time(&clk_end);

    //use the last value once to make the compiler keep it (smart compilers see that val is not used and might alleviate it altogether from the executable)
    val=val/3; 

    //calculate the difference in nanoseconds, microseconds, and seconds
    long loop_nanosec=getclock_diff_ns(clk_start,clk_end);
    long loop_microsec=getclock_diff_us(clk_start,clk_end);
    double loop_sec=getclock_diff_s(clk_start,clk_end);

    printf("Loop 1 time %ld nanosec = %ld microsec = %lf sec\n",loop_nanosec,loop_microsec,loop_sec);

    // repeat with a 16 times longer loop
    K=16*K;
    printf("%d iterations:\n",K); 

    my_clock_get_time(&clk_start);
    for (i=0;i<K;i++)
        val=2*i;
    my_clock_get_time(&clk_end);
    
    val=val/3;
    loop_nanosec=getclock_diff_ns(clk_start,clk_end);
    loop_microsec=getclock_diff_us(clk_start,clk_end);
    loop_sec=getclock_diff_s(clk_start,clk_end);

    printf("Loop 2 time %ld nanosec = %ld microsec = %lf sec\n",loop_nanosec,loop_microsec,loop_sec);

    return 0;
}