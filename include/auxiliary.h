/*
 * auxiliary.h
 *
 *  Created on: Apr 26, 2012
 *      Author: Manos Athanassoulis
 *
 */

#ifndef AUX_H_
#define AUX_H_

#include <time.h>
#include <stdio.h>



//#define __USE_GETTIMEOFDAY
#ifdef __USE_GETTIMEOFDAY
//#warning "Using gettimeofay()"
#include <sys/time.h>
typedef struct timeval my_clock;
#else
//#warning "Using clock_gettime()"
typedef struct timespec my_clock;
#endif

/*For us and s accuracy of timings*/
// print the value of the high precision captured timestamp
void my_print_clock(my_clock clk);

//capture current high-precision timestamp
int my_clock_get_time(my_clock *clk);

//calculate time difference clk2-clk1 in microseconds (long)
long getclock_diff_us(my_clock clk1, my_clock clk2);

//calculate time difference clk2-clk1 in nanoseconds (long)
long getclock_diff_ns(my_clock clk1, my_clock clk2);

//calculate time difference clk2-clk1 in seconds (double)
double getclock_diff_s(my_clock clk1, my_clock clk2);








#endif /* AUX_H_ */
